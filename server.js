const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const path = require("path");
const cors = require("cors");

app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var games = require("./produtos.json");

var DB = {
  games,
};

//console.log(DB);

app.get("/games", (req, res) => {
  res.statusCode = 200;
  res.json(DB.games);
});

app.get("/games/:nome", (req, res) => {
  var nome = req.params.nome;

  var produtosEncontrados = [];

  console.log(nome);

  for (x = 0; x < DB.games.length; x++) {
    var titulo = DB.games[x].title;

    //console.log(titulo);

    if (titulo.includes(nome)) {
      produtosEncontrados.push(DB.games[x]);
    }
  }

  //console.log(produtosEncontrados);

  res.statusCode = 200;
  res.json(produtosEncontrados);
});

app.get("/carrinho/:id", (req, res) => {
  var id = req.params.id;

  console.log(id);

  var game = DB.games.find((g) => g.id == id);

  console.log(game);

  res.statusCode = 200;
  res.json(game);
});

app.listen(3005, () => {
  console.log("API RODANDO!");
});
