var produtos = JSON.parse(localStorage.getItem("produtos") || "[]");

console.log(produtos);

new Vue({
  el: "#app",

  data: {
    produtos,

    tax: 5,
    promotions: [
      {
        code: "SUMMER",
        discount: "50%",
      },
      {
        code: "AUTUMN",
        discount: "40%",
      },
      {
        code: "WINTER",
        discount: "30%",
      },
    ],
    promoCode: "",
    discount: 0,
  },
  computed: {
    itemCount: function () {
      return produtos.length;
    },
    subTotal: function () {
      var subTotal = 0;

      for (var i = 0; i < this.produtos.length; i++) {
        subTotal += this.produtos[i].price;
      }

      return subTotal;
    },
    discountPrice: function () {
      return (this.subTotal * this.discount) / 100;
    },
    totalPrice: function () {
      return this.subTotal - this.discountPrice + this.tax;
    },
  },
  filters: {
    currencyFormatted: function (value) {
      return Number(value).toLocaleString("pt-BR", {
        style: "currency",
        currency: "BRL",
      });
    },
  },
  methods: {
    updateQuantity: function (index, event) {
      var product = this.produtos[index];
      var value = event.target.value;
      var valueInt = parseInt(value);

      // Minimum quantity is 1, maximum quantity is 100, can left blank to input easily
      if (value === "") {
        product.quantity = value;
      } else if (valueInt > 0 && valueInt < 100) {
        product.quantity = valueInt;
      }

      this.$set(this.produtos, index, product);
    },
    checkQuantity: function (index, event) {
      // Update quantity to 1 if it is empty
      if (event.target.value === "") {
        var product = this.produtos[index];
        product.quantity = 1;
        this.$set(this.produtos, index, product);
      }
    },

    checkPromoCode: function () {
      for (var i = 0; i < this.promotions.length; i++) {
        if (this.promoCode === this.promotions[i].code) {
          this.discount = parseFloat(
            this.promotions[i].discount.replace("%", "")
          );
          return;
        }
      }

      alert("Esse cupom não e valido!");
    },
  },
});
