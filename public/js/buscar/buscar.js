function dadosPesquisa() {
  var produto = $("#pesquisa").val();

  console.log(produto);

  axios
    .get(`http://localhost:3005/games/${produto}`)
    .then((response) => {
      $(".grid").empty();

      console.log(response.data);

      var dados = response.data;

      for (x = 0; x < response.data.length; x++) {
        $(".grid").append(
          `     <div id="cardForm">
        <div
          style="
            background-image: url('${dados[x].img_url}');
          "
          id="imgFundo"
        >
          <div id="caixaPreco">
            <p><span>R$: </span>${dados[x].price}</p>
          </div>
        </div>
        <div id="imformacoes">
          <h3 id="nomeProduto">
            ${dados[x].title}
          </h3>
          <p id="descricaoProduto">
            ${dados[x].description}
          </p>
          <button
            onclick="showToast('😍 Adicionado com Sucesso!')"
            id="botaoComprar"
             onclick="adicionarCarrinho(${dados[x].id})"
          >
            Comprar
          </button>
        </div>
      </div>`
        );
      }
    })
    .catch((error) => {
      console.log(error);
    });
}
