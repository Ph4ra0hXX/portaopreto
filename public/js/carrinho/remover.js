function removeItem(index) {
  console.log(index);

  var produtos = JSON.parse(localStorage.getItem("produtos") || "[]");

  produtos = produtos.filter(function (produtos) {
    return produtos.id !== index;
  });

  localStorage.setItem("produtos", JSON.stringify(produtos));

  document.location.reload(true);
}
