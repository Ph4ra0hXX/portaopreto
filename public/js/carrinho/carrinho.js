var listaDeProdutosDoCarrinho = [];
var produtos;

function adicionarCarrinho(id) {
  console.log(id);

  axios
    .get(`http://localhost:3005/carrinho/${id}`)
    .then((response) => {
      listaDeProdutosDoCarrinho.push(response.data);
      localStorage.setItem(
        "produtos",
        JSON.stringify(listaDeProdutosDoCarrinho)
      );

      produtos = JSON.parse(localStorage.getItem("produtos") || "[]");

      console.log(produtos);
    })
    .catch((error) => {
      console.log(error);
    });
}
